﻿import React, { Component } from 'react';

export class Login extends React.Component {
    static DisplayName = Login.name;
    constructor(props) {
        super(props);
        this.loading = this.loading.bind(this);
        this.state = {
            username: '',
            password: '',
            btntxt: 'ورود به سیستم',
            loadingclass:'none',
        }
    }
   
    loading() {
        if( this.state.loadingclass=='none')
            this.setState({
                btntxt: ' بارگذاری...',
                loadingclass: ''
            });
        else
            this.setState({
                btntxt: 'ورود به سیستم',
                loadingclass: 'none'
            });
    }
    render() {
        return (
            <div className="container">
                <form className="col-sm-6 offset-sm-3 mt-5">
                    <div className="card card-primary">
                        <div className="card-header bg-dark text-white">درگاه سیستم</div>
                        <div className="card-body">
                            <div className="form-group">
                                <div className="input-group">
                                    <div className="input-group-append">
                                        <span className="input-group-text"><i className="fa fa-user"></i></span>
                                    </div>

                                    <input id="username" className="form-control" name="username" type="text" placeholder="نام کاربری یا کد ملی" required="" />
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <div className="input-group-append">
                                        <span className="input-group-text"><i className="fa fa-lock"></i></span>
                                    </div>
                                    <input autoComplete="off" className="form-control" name="password" type="password" placeholder="کلید واژه" id="txtpassword" required="" />
                                </div>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-warning btn-block" onClick={this.loading} type="button">
                                    <span className="spinner-border spinner-border-sm" style={{ display: this.state.loadingclass }}></span>
                                    {this.state.btntxt}
                                </button>
                            </div>
                            <div className="form-group">
                                <a href="#" className="card-link" >
                                    فراموشی گذرواژه
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
